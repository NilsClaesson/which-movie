import React, { Component } from "react";
import axios from "axios";
import "react-bulma-components/dist/react-bulma-components.min.css";
import Header from "./components/Header";
import Footer from "./components/Footer";
import NewsTiles from "./components/NewsTiles";

class App extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      articles: [],
      country: "country=se&",
      category: "",
      keyword: "",
    };
  }

  componentDidMount() {
    this.getNews();
  }

  async getNews() {
    const proxyUrl = "https://cors-anywhere.herokuapp.com/";

    await axios
      .get(
        `${proxyUrl}` +
          "https://newsapi.org/v2/top-headlines?" +
          this.state.country +
          this.state.category +
          "q=" +
          this.state.keyword +
          "&" +
          "pageSize=100&" +
          "apiKey=7b1a3f33d36d4d259c6f39b3a3eed227"
      )
      .then((json) =>
        json.data.articles.map((result, index) => ({
          title: result.title,
          description: result.description,
          author: result.author,
          url: result.url,
          urlToImage: result.urlToImage,
          publishedAt: result.publishedAt,
          id: result.index,
        }))
      )
      .then((newData) => this.setState({ articles: newData }))
      .catch((error) => console.log(error));

    window.scrollTo(0, 0);
  }

  handleChange(event) {
    const { name, value, type } = event.target;
    type === "text"
      ? this.setState({ [name]: value })
      : this.setState({ [name]: value }, this.getNews);
  }

  render() {
    const { articles } = this.state;
    return (
      <div className="has-background-light">
        <section className="fixed-height">
          <Header handleChange={this.handleChange} data={this.state} />
        </section>
        <br />
        <div className="columns is-mobile is-centered">
          <div className="column is-four-fifths  is-gapless">
            <p className="bd-notification is-primary"></p>
            <NewsTiles articles={articles} />
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default App;
