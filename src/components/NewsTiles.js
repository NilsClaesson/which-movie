import React from "react";
import NewsCard from "./NewsCard";

function NewsTiles(props) {
  const { articles } = props;
  let articleIndex = 0;

  function displayArticles() {
    let displayedArticles = [];

    for (var i = 0; i < 4; i++) {
      if (articles.length > articleIndex) {
        displayedArticles.push(
          <div className="tile is-parent" key={articleIndex}>
            <div className="tile is-child">
              {<NewsCard article={articles[articleIndex]} />}
            </div>
          </div>
        );
        articleIndex++;
      }
    }
    return displayedArticles;
  }

  return (
    <div>
      <div className="tile is-ancestor">{displayArticles()}</div>
      <div className="tile is-ancestor">{displayArticles()}</div>
      <div className="tile is-ancestor">{displayArticles()}</div>
      <div className="tile is-ancestor">{displayArticles()}</div>
    </div>
  );
}

export default NewsTiles;
