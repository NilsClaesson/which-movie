import React from "react";

function NewsCard(props) {
  const { article } = props;

  return (
    <div>
      <a target="_blank" rel="noopener noreferrer" href={article.url}>
        <div className="card">
          <div className="card-image">
            <figure className="image is-4by3">
              <img src={article.urlToImage} alt="" />
            </figure>
          </div>
          <div className="card-content">
            <div className="media">
              <div className="media-content">
                <p className="title is-size-6 title-font">{article.title}</p>
              </div>
            </div>
            <div className="content is-size-7">
              {article.description !== null &&
                article.description.slice(0, 200)}
              <br />
            </div>
          </div>
        </div>
      </a>
    </div>
  );
}

export default NewsCard;
