import React from "react";
import "../style.css";

function Header(props) {
  return (
    <div className="sticky-header">
      <div className="hero is-small has-background-white is-bold header-border">
        <div className="hero-body">
          <section>
            <div className="level">
              <div className="level-item">
                <h1 className="title header-font is-size-1 has-text-danger has-text-centered">
                  • Newsy •
                </h1>
              </div>
            </div>
          </section>
          <br />
          <section>
            <div className="column">
              <div className="columns is-half is-centered">
                <div className="level is-mobile">
                  <div className="level-item">
                    <div className="field">
                      <div className="control">
                        <div className="select is-primary is-small">
                          <select
                            value={props.data.value}
                            defaultValue={"country=se&"}
                            name="country"
                            onChange={props.handleChange}
                          >
                            <option value="country=se&">Sweden</option>
                            <option value="country=us&">USA</option>
                            <option value="country=gb&">England</option>
                            <option value="country=de&">Germany</option>
                            <option value="country=fr&">France</option>
                            <option value="country=jp&">Japan</option>
                            <option value="country=in&">India</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="level-item">
                    <div className="field">
                      <div className="control">
                        <div className="select is-primary is-small">
                          <select
                            value={props.data.value}
                            defaultValue=""
                            name="category"
                            onChange={props.handleChange}
                          >
                            <option value="">All</option>
                            <option value="category=science&">Science</option>
                            <option value="category=technology&">Tech</option>
                            <option value="category=entertainment&">
                              Culture
                            </option>
                            <option value="category=business&">Business</option>
                            <option value="category=health&">Health</option>
                            <option value="category=sports&">Sports</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="level-item">
                    <div className="control">
                      <div className="field has-addons">
                        <input
                          name="keyword"
                          className="input is-primary is-small input-size"
                          type="text"
                          placeholder="Tag"
                          value={props.data.keyword}
                          onChange={props.handleChange}
                        />
                        <button
                          className="button is-primary is-small"
                          onClick={props.handleChange}
                        >
                          >
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  );
}

export default Header;
