import React from "react";

function Footer() {
  return (
    <div>
      <footer className="footer">
        <div className="content has-text-centered">
          <p>
            <strong>Newsy</strong> by Nils Claesson
          </p>
          <p>
            <strong>2020</strong>
          </p>
        </div>
      </footer>
    </div>
  );
}

export default Footer;
