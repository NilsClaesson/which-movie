# Newsy

Newsy fetches news from newsapi.org and displays them in a grid layout for an easy overview. News can be fetched according to country and category and then filtered to include specific keywords. For example: Swedish science headlines about astronomy. The project is built in React and uses Bulma as the CSS framework.

## Demo

The app is deployed with Vercel and is running at [newsy.now.sh](https://newsy.now.sh/)

## Getting Started

Clone the project and install:

- [React](https://reactjs.org/) - The web framework used
- [React Bulma Components](https://react-bulma.dev/en) - The CSS framework used
- [Axios](https://github.com/axios/axios) - For making API calls

Run the app in development mode with:

```
yarn start
```

## Authors

- **Nils Claesson** - _Initial work_ - [Profile](https://gitlab.com/NilsClaesson)
